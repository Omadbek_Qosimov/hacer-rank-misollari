def swap_case(s):
   
    b=str()
    for i in range(len(s)):
        if ord(s[i])>=32 and ord(s[i])<=64:
            b+=s[i]
        else:
            if s[i]==s[i].title():
                
                b+=s[i].lower() 
            if s[i]==s[i].lower():
                b+=s[i].title()   
        
    return b

if __name__ == '__main__':
    s = input()
    result = swap_case(s)
    print(result)